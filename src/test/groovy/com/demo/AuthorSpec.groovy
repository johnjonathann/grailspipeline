package com.demo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Author)
class AuthorSpec extends Specification {

    def setup() {
	def author = new Author(name:"Bhushan").save(flush:true)
    }

    def cleanup() {
	Author.findAll().each{it.delete(flush:true)}
    }

    void "test creation"() {
        expect:
            Author.findAll().size() == 1
    }
}
