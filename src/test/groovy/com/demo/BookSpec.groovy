package com.demo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Book)
class BookSpec extends Specification {

    def setup() {
	mockDomain(Author)
        def author = new Author(name: "Bhushan").save(flush: true)
        def book = new Book(title: "Almost Famous", isbn: "213123123", author: "1").save(flush: true)
    }

    def cleanup() {
        Book.findAll().each {it.delete(flush:true)}
        Author.findAll().each {it.delete(flush: true)}
    }

    void "test creation"() {
        expect:
            Book.findAll().size() == 1
            Author.findAll().size() == 1
    }
}
