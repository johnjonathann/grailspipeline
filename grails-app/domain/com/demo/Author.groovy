package com.demo

class Author {
    
    static hasMany = [books: Book]

    String name

    static constraints = {
        name nullable: false, blank: false
    }
}
